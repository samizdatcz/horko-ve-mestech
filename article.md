---
title: "Mapa pražského horka: Proč jsou v létě Vinohrady příjemnější než Smíchov"
perex: "Hlavní město hledá cesty, jak se vypořádat s tropickými vedry"
description: "Česká města hledají způsob, jak se přizpůsobit nárůstu teploty."
authors: ["Jan Cibulka"]
published: "10. srpna 2016"
socialimg: https://interaktivni.rozhlas.cz/horko-ve-mestech/media/socimg.png
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "horko-ve-mestech"
libraries: [jquery, highcharts,  https://interaktivni.rozhlas.cz/tools/highcharts-technical-indicators/0.0.1.min.js, https://js.arcgis.com/3.17/]
styles: [https://js.arcgis.com/3.17/esri/css/esri.css]
recommended:
  - link: https://interaktivni.rozhlas.cz/horko-v-brne/
    title: Mapa horkého Brna: Na nádražích a v průmyslových zónách je až o deset stupňů tepleji
    perex: Průmyslové zóny, nádraží a nákupní centra se v letních dnech ohřejí na teploty i o deset stupňů vyšší, než jaké jsou v parcích a zatravněných oblastech. Ukazují to snímky ze satelitu Landsat 8, které umožňují odhadnout teplotu povrchu.
    image: https://interaktivni.rozhlas.cz/horko-v-brne/media/socimg.png
---

Během teplejšího letního dne se pražské náměstí Republiky už během dopoledne ohřeje na čtyřicet stupňů Celsia, sousední Masarykovo nádraží sálá padesátkou. Přitom nedaleké Riegerovy sady si drží snesitelnou třicítku. Praha nyní začíná hledat způsoby, jak přetopenému centru ulevit. Už nyní je jasné, že pomohou hlavně stromy.

Tropických dní, tedy těch, při kterých maximální denní teplota překročí 30 °C, přibývá v celém Česku. Meteorologická stanice v pražské Ruzyni jich loni zaznamenala 24. Podle [výpočtů](https://www.natur.cuni.cz/geografie/fyzicka-a-geoekologie/aktuality/sucho-v-ceskych-zemich-minulost-soucasnost-a-budoucnost) vědců z Akademie věd, kteří si vzali na pomoc upravený meteorologický model [ALADIN](https://cs.wikipedia.org/wiki/ALADIN), se má jejich počet do roku 2050 zvýšit o polovinu.

Ve velkých městech asfaltové a betonové plochy vytvářejí tepelné ostrovy, z nichž sálá horko i v noci. V průměru je během léta v noční Praze o 2,4 °C více než na venkově. Vyšší teploty znepříjemňují obyvatelům život a mají [negativní vliv na zdraví](http://www.rozhlas.cz/pardubice/zpravodajstvi/_zprava/zachranari-maji-pri-horkych-letnich-dnech-vic-prace-nez-obvykle--1513246).

Rostoucí trend je patrný z přehledu tropických dní v Praze, jejichž počet od roku 1775 narostl asi o třetinu. Extrémní byly v poslední době roky 1994 a 2003, během kterých se maximální teplota vyšplhala ke třicítce skoro ve třech desítkách dní.

<aside class="big">
   <div style="height:400px;" class="trop_pha"></div>
</aside>

Český rozhlas zjišťoval teplotní rozdíly přímo uvnitř Prahy. Na základě snímků ze satelitu [Landsat 8](http://landsat.usgs.gov/landsat8.php) zpracoval odhad teploty městských povrchů během dopoledne 24. června 2016. Tohle konkrétní datum bylo vybráno kvůli vysoké denní teplotě a minimální oblačnosti, kdy mraky satelitu nezakrývaly výhled.

*Satelit je schopen zachytit [elektromagnetické záření](https://cs.wikipedia.org/wiki/Elektromagnetick%C3%A9_spektrum) z různých typů povrchů. Následnými výpočty je možné přibližně určit jejich teplotu ve chvíli snímkování. Zjištěné teploty jsou přitom vyšší, než jaké známe z předpovědí počasí: meteorologické teploměry měří teplotu ve stínu, zatímco satelit zachytil přibližné hodnoty, které měly střechy a silnice vystavené slunci.*

<aside class="small"> <figure> <img src="./media/scrn_nadrazi.jpg" width="300"> </figure> <figcaption><i>Přehřáté okolí Masarykova nádraží v kontrastu s nedalekým parkem</i></figcaption> </aside>

Jak ukazuje následující mapa, nejhorší situace je u velkých tmavých ploch, jakými jsou [brownfieldy](https://cs.wikipedia.org/wiki/Brownfield), tedy v metropoli typicky nádraží, dále pak velká parkoviště či střechy továrních hal.

Chladněji je naopak u řeky nebo v blízkosti stromů: Vltava je schopná ochladit stavby ve svém okolí asi o čtyři stupně, pocítí to lidé bydlící v první či druhé řadě domů ve směru od říčního břehu. Dva až tři stupně ale zvládnou srazit i malé stromy a trávník mezi domy. To je důvod, proč je na Vinohradech asi o 4 °C příjemněji než na Smíchově. Velké porosty, jako lesoparky, si udržují až o osm stupňů nižší teploty než zástavba v jejich okolí.

<aside id="mapa" class="big">
	<div id="search"></div>
	<div style="height:600px;" id="map">
</aside>

Hlavní město teď zkoumá, jak s rostoucími teplotami bojovat. Letos v březnu se Praha [připojila](http://www.praha.eu/jnp/cz/o_meste/magistrat/tiskovy_servis/tiskove_zpravy/pristoupeni_hlavniho_mesta_prahy_k.html) k iniciativě Mayors Adapt, kde odpovědi na stejné otázky hledá i [Brno](http://urbanadapt.cz/cs/adaptace-mesta-brna-na-klimaticke-zmeny) či [Plzeň](http://urbanadapt.cz/cs/adaptace-mesta-plzne-na-klimaticke-zmeny). V Praze má projekt na starosti [Institut plánování a rozvoje](http://www.iprpraha.cz/), který by měl [výsledky](http://www.iprpraha.cz/adaptacnistrategie) představit v první polovině roku 2017. Z navržených opatření si pak politici vyberou, do kterých úprav město investuje.

Do té doby si Praha musí vystačit se [strategickým plánem](http://www.iprpraha.cz/clanek/83/co-je-strategicky-plan), který předpokládá výsadbu další zeleně. Kromě horka mají stromy pomáhat i se snižováním prašnosti a obecně zpříjemňovat Pražanům pobyt venku. Nové [stavební předpisy](http://www.iprpraha.cz/coprinasipsp) už teď nařizují, že každá nová ulice širší než 12 metrů musí být navržena tak, aby v ní bylo možné vysadit stromořadí. Týká se to zejména uložení inženýrských sítí: potrubí a kabely musí být zahloubené tak, aby se k nim vměstnaly i kořeny stromů.

U staré zástavby je situace komplikované, přeložení všech trubek by bylo extrémně drahé. V ulici Šlikova na Břevnově proto testují truhlíky s osazenými stromy. Oproti nechvalně proslulým [betonovým žlabům ze Smíchova](http://www.rozhlas.cz/zpravy/regiony/_zprava/na-prazsky-smichov-se-vrati-kontroverzni-truhliky-budou-mit-novou-podobu--1501194) jsou tyto ze dřeva a navíc výrazně menší, na chodnících tak nepřekáží.

<aside class="big"> <figure> <img src="./media/slikova_praha.png" width="1000"> </figure> <figcaption>Truhlík v ulici Šlikova, foto <a href="https://mapy.cz/s/Xjoc" target="_blank">Mapy.cz</a> </figcaption> </aside>

<aside class="small"> <figure> <img src="./media/parkoviste_plzen.jpg" width="300"> </figure> <figcaption><a href="http://urbanadapt.cz/cs/publikace-adaptace-na-zmenu-klimatu-ve-mestech" target="_blank">Adaptace na změnu klimatu ve městech, s. 78</a> </figcaption> </aside>

Na některých místech projektanti nahrazují tmavý asfalt, který se snadno rozpálí na teploty přes 50 °C, jinými materiály. V úvahu připadá světlejší beton nebo známé tvárnice s otvory, jak ukazuje příklad z rekonstrukce plzeňských Štruncových sadů. Skrze ně prorůstá tráva a zároveň se vsakuje přebytečná voda. Kromě vedra takové parkoviště lépe zvládá i přívalové deště.

Hlavní město nyní otevřelo nový [park Maxe van der Stoela](http://www.praha.eu/jnp/cz/o_meste/zivot_v_praze/zivotni_prostredi/velky_park_slouzi_nejen_detem.html) na Praze 6. Mezi stromy a trávou protéká potok Brusnice, který napájí zdejší rybník. Dětské hřiště je upraveno tak, aby se děti pohodlně dostaly k vodě. Zda budou podobně vypadat i další pražské parky, naznačí chystaná [adaptační strategie](http://www.iprpraha.cz/adaptacnistrategie) a zejména pak zájem Pražanů.
